namespace :trace do
  desc "run"
  task run: :environment do  
    trace = TracePoint.new(:call) do |tp|
      if tp.path.include? 'ibm_db_adapter.rb'
        printf "%s.%s:%-2d.%s.%s\n", tp.defined_class, tp.method_id, tp.lineno, Time.now.to_i, Thread.current.object_id    
      end
    end
    trace.enable
    puts "Query results..."
    User.all.each do |u|
      puts u.name
    end
  end
end
