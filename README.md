#README

##Setup
CREATE COLLECTION ab1_d

rake db:migrate

rails s -p5000

http://host:5000

##Trace
There is a rake task named `trace:run` that uses Ruby's [`TracePoint`](http://ruby-doc.org/core-2.1.1/TracePoint.html) to log ibm_db_adapter.rb calls.  To invoke do the following:

```
$ cd ibmdb_fetchbug
$ rake trace:run
```

# Hacking The ibm_db Adapter
If you need to modify `ibm_db_adapter.rb` for debugging purposes then it is best to do it on a copy vs. the version stored in `/PowerRuby`.  You can do that with the following commands:

```
mkdir -p /ruby/gemsets/ibmdb_fetchbug/gems
cp -R /PowerRuby/prV2R0/lib/ruby/gems/2.0.0/gems/ibm_db-2.5.14-powerpc-aix-6 /ruby/gemsets/ibmdb_fetchbug/gems/
cp /PowerRuby/prV2R0/lib/ruby/gems/2.0.0/specifications/ibm_db-2.5.14-powerpc-aix-6.gemspec /ruby/gemsets/ibmdb_fetchbug/specifications/
```
Then you also need to modify your `GEM_PATH` so it picks up the new `ibm_db` Gem.
```
export GEM_PATH=/ruby/gemsets/ibmdb_fetchbug:/PowerRuby/prV2R0/lib/ruby/gems/2.0.0
```