RAILS_ENV=${RAILS_ENV:=development}
mkdir -p /ruby/gemsets/ibmdb_fetchbug-$RAILS_ENV
export GEM_HOME=/ruby/gemsets/ibmdb_fetchbug-$RAILS_ENV
export GEM_PATH=/ruby/gemsets/ibmdb_fetchbug-$RAILS_ENV:/PowerRuby/prV2R1/lib/ruby/gems/2.1.0

echo "RAILS_ENV=$RAILS_ENV"
echo "GEM_HOME=$GEM_HOME"
echo "GEM_PATH=$GEM_PATH"
